#include <regex>
#include <gtest/gtest.h>
#include <date/tz.h>
#include <chrono>

using namespace std;

TEST(TestDateParsing, ParseFromStringWithTZ) {
	const string in = "2021-09-09T14:41:04.000-05:00";

	// 1. remove ".000"
	static const regex pattern { R"((.*?)\.\d+(.*))" };
	const string out = regex_replace(in, pattern, "$1$2");

	istringstream istr { out };
	ostringstream ostr;
	ostringstream lostr;

	date::utc_time<chrono::milliseconds> utc_time;
	date::local_time<chrono::milliseconds> local_time;

	// 2. convert string to UTC time (takes timezone into consideration)
	istr >> date::parse("%FT%T%z", utc_time);
	cout << "utc: " << utc_time << "\n";

	// 3. convert UTC time to system time
	auto sys_time = to_sys_time(utc_time);

	// 4. convert system time to local time using current timezone set in the system
	auto user_time = date::current_zone()->to_local(sys_time);

	// 5. convert dates to string and print them
	date::to_stream(ostr, "%FT%T%z", sys_time);
	cout << "sys: " << ostr.str() << "\n";
	date::to_stream(lostr, "%FT%T%z", user_time);
	cout << "local: " << lostr.str() << "\n";
}

TEST(TestDateParsing, TZToTimestampAndBackToLocal) {
//	const string in = "2021-08-31T13:56:54.000-05:00";
	const string in = "2021-09-09T14:41:04.000-05:00";

	istringstream istr { in };
	ostringstream ostr;

	date::utc_time<chrono::milliseconds> utc_time;
	istr >> date::parse("%FT%T%z", utc_time);
	auto timestamp = utc_time.time_since_epoch().count();

	//

	auto dur = std::chrono::milliseconds(timestamp);
	auto time_point = std::chrono::time_point<date::utc_clock, std::chrono::milliseconds>(dur);
	auto sys_time = to_sys_time(time_point);
	auto local_time = date::current_zone()->to_local(sys_time);

	date::to_stream(ostr, "%FT%T", local_time);
	cout << ostr.str() << "\n";

	// This is commented out, because date::current_zone() changes for me from CET to CEST,
	// but for CEST it should be:
	//
	// ASSERT_EQ("2021-08-31T20:56:54.000", ostr.str());
	//
	// and for CET:
	//
	// ASSERT_EQ("2021-08-31T19:56:54.000", ostr.str());
}

TEST(TestDateParsing, PrintTZSpec) {
	auto z = date::current_zone();
}
