#pragma once

#include <memory>
#include <optional>
#include "helpers.h"

namespace db {

struct DatabasePrivate;

struct RawLobstersPost final {
public:
	uint64_t parentId;
	uint64_t id;
	std::string shortId;
	std::string parentShortId;
	std::string author;
	std::string body;
	std::string title;
	uint64_t timestamp;
	bool bodyRequested = false;
};

class Database final {
public:
	explicit Database(const std::string& fname);
	virtual ~Database() = default;

	static inline Creator<Database> create;

	void sql(const std::string& expr);

	std::pair<uint64_t, uint64_t> getMinMaxForTable(const std::string& table);
	std::pair<uint64_t, uint64_t> getMinMaxAfterTSForTable(uint64_t ts, const std::string& table);
	std::optional<std::string> getCacheForURL(const std::string& url);
	std::optional<uint64_t> getArticleCountForTable(const std::string& url);
	void putCacheForURL(const std::string& url, const std::string& data);

	std::optional<uint64_t> getLobstersIdByShortId(const std::string& shortId);

	uint64_t putLobstersPost(
		uint64_t parentId,
		const std::string& shortId,
		const std::string& author,
		const std::string& title,
		const std::string& body,
		uint64_t timestamp
	);

	uint64_t putLobstersPostInStaging(
		const std::string& parentShortId,
		const std::string& shortId,
		const std::string& author,
		const std::string& title,
		const std::string& body,
		uint64_t timestamp
	);

	void eachStagingPost(std::function<void (RawLobstersPost&)> closure);
	void clearStaging();

	std::optional<RawLobstersPost> getLobstersPost(uint64_t id, bool readBody);
	std::vector<RawLobstersPost> getLobstersPostAfterTimestamp(uint64_t ts, bool readBody);

	void purgeCache();

private:
	std::shared_ptr<DatabasePrivate> p;
};

}

using DatabaseRef = std::shared_ptr<db::Database>;