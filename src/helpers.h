#pragma once

#include <memory>
#include <cctype>
#include <algorithm>
#include <sstream>
#include <optional>
#include <limits>
#include <array>

#include <fmt/core.h>
#include <date/tz.h>

template <typename A>
struct Creator final {
	template <typename... Args>
	std::shared_ptr<A> operator() (Args&&... args) {
		return std::make_shared<A>(std::forward<Args>(args)...);
	}
};

static std::string strip(const std::string& inpt) {
	if (inpt.empty())
		return "";

	auto start_it = inpt.begin();
	auto end_it = inpt.rbegin();
	while (std::isspace(*start_it) || *start_it == '\n' || *start_it == '\r')
		++start_it;
	while (std::isspace(*end_it) || *end_it == '\n' || *end_it == '\r')
		++end_it;
	return { start_it, end_it.base() };
}

static void strip(std::string& inpt) {
	inpt = strip(static_cast<const std::string&>(inpt));
}

static void uppercase(std::string& input) {
	std::transform(input.begin(), input.end(), input.begin(), ::toupper);
}

static std::vector<std::string> split(const std::string& s, char delim) {
	std::vector<std::string> result;
	std::stringstream ss(s);
	std::string item;

	while (getline (ss, item, delim)) {
		result.push_back(item);
	}

	return result;
}

static std::optional<std::pair<uint64_t, uint64_t>> decodeRange(const std::string& range) {
	if (std::all_of(range.begin(), range.end(), ::isdigit)) {
		const auto id = std::strtoull(range.c_str(), nullptr, 10);
		return std::make_pair(id, id);
	}

	const auto tokens = split(range, '-');
	if (tokens.size() == 1) {
		const uint64_t number = std::strtoull(tokens[0].c_str(), nullptr, 10);

		if (range.ends_with('-')) {
			return std::make_pair(number, std::numeric_limits<uint64_t>::max());
		} else if (range.starts_with('-')) {
			return std::make_pair(0, number);
		} else {
			return std::nullopt;
		}
	} else if (tokens.size() == 2) {
		const uint64_t first = std::strtoull(tokens[0].c_str(), nullptr, 10);
		const uint64_t second = std::strtoull(tokens[1].c_str(), nullptr, 10);

		if (second >= first) {
			return std::make_pair(first, second);
		} else {
			return std::nullopt;
		}
	} else {
		return std::nullopt;
	}
}

static std::string asDateString(uint64_t timestamp) {
	using namespace date;
	using namespace std::chrono;

	static std::array<std::string, 7> weekdays {
		"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"
	};

	typedef std::chrono::time_point<date::utc_clock, std::chrono::milliseconds>	tpMilliseconds;

	auto tp_milli = tpMilliseconds(std::chrono::milliseconds(timestamp));
	auto sys_time = to_sys_time(tp_milli);
	auto wd = date::weekday { floor<date::days>(sys_time) };

	std::ostringstream ostr;
	date::to_stream(ostr, "%a, %e %B %Y %T +0000 (UTC)", tp_milli); // current_zone()->to_local(sys_time));
	//return fmt::format("{}, {}", weekdays[wd.iso_encoding() - 1], ostr.str());
	return fmt::format("{}", ostr.str());
}

template <typename Iterator>
std::string join(Iterator begin, Iterator end, const std::string& separator = " ") {
	std::ostringstream o;

	if(begin != end) {
		o << *begin++;

		for(; begin != end; ++begin)
			o  << separator << *begin;
	}

	return o.str();
}