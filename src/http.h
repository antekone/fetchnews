#pragma once

#include <memory>
#include <algorithm>
#include "helpers.h"
#include "db.h"

namespace http {

struct HTTPClientPrivate;
class Client;

struct Result final {
	std::string body;
	long statusCode;

	static inline Creator<Result> create;

	explicit operator bool() const {
		return statusCode == 200;
	}

	[[nodiscard]] bool ok() const { return static_cast<bool>(this); }

	std::string serialize();
	static std::shared_ptr<Result> deserialize(const std::string& input);
};

using ResultRef = std::shared_ptr<Result>;

class Client final {
public:
	Client();
	virtual ~Client() = default;

	static inline Creator<Client> create;

	ResultRef get(const std::string& url);
	ResultRef get(const std::string& url, std::shared_ptr<db::Database>& db);

private:
	std::shared_ptr<HTTPClientPrivate> p;
};

}