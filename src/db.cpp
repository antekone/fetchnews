#include <memory>
#include <map>
#include <spdlog/spdlog.h>

#include <sqlite3.h>

#include "errors.h"
#include "db.h"
#include "helpers.h"

namespace db {

struct DatabasePrivate final {
	sqlite3* sql = nullptr;

	explicit DatabasePrivate(const std::string& fname) {
		int code;
		if (SQLITE_OK != (code = sqlite3_open(fname.c_str(), &sql))) {
			throw Error::Database::formatted(
				"can't open database: '{}': error {}", fname, code);
		}
	}

	~DatabasePrivate() {
		std::ignore = sqlite3_close(sql);
	}
};

using DatabasePrivateRef = std::shared_ptr<DatabasePrivate>;

static int stepWrapper(sqlite3_stmt* stmt, const char* sql) {
	int ret;

	spdlog::trace("running: {}", sql);

	while (true) {
		ret = sqlite3_step(stmt);

		if (ret == SQLITE_BUSY)
			continue;
		else if (ret == SQLITE_DONE || ret == SQLITE_ROW)
			break;
		else
			throw Error::Database::formatted("error {} during SQL: '{}': {}", ret, sql,
											 sqlite3_errstr(ret));
	}

	return ret;
}

class Statement;
class RowMapper final {
public:
	bool next() {
		if (!hasRows) {
			return false;
		} else if (firstRow) {
			firstRow = false;
			return true;
		} else {
			int ret = db::stepWrapper(stmt, origSQL.c_str());
			return ret != SQLITE_DONE;
		}
	}

	template <typename A>
	A get(const std::string& fieldName) {
		cacheColumns();

		auto index = columnCache.find(fieldName);
		if (index == columnCache.end())
			throw Error::Database::formatted("No such field in result set: {}", fieldName);

		A data;
		unbind(index->second, &data);
		return data;
	}

	void unbind(int index, std::string* out) {
		*out = reinterpret_cast<const char*>(sqlite3_column_text(stmt, index));
	}

	void unbind(int index, int* out) {
		*out = sqlite3_column_int(stmt, index);
	}

	void unbind(int index, uint64_t* out) {
		*out = sqlite3_column_int64(stmt, index);
	}

	void unbind(int index, long* out) {
		*out = sqlite3_column_int64(stmt, index);
	}

protected:
	friend class Statement;

	std::shared_ptr<Statement> refHolder;

	explicit RowMapper(std::string origSQL, sqlite3_stmt* stmt, bool unlocked)
		: origSQL(std::move(origSQL)), stmt(stmt), hasRows(unlocked) {

	}

private:
	bool hasRows = false;
	bool firstRow = true;
	sqlite3_stmt* stmt;
	std::string origSQL;
	std::map<std::string, int> columnCache;

	void cacheColumns() {
		if (columnCache.empty()) {
			for (int i = 0, len = sqlite3_column_count(stmt); i < len; i++) {
				std::string name = sqlite3_column_name(stmt, i);
				columnCache[name] = i;
			}
		}
	}
};

class Statement final : public std::enable_shared_from_this<Statement> {
public:
	static inline Creator<Statement> create;

	Statement(DatabasePrivateRef& p, const std::string& sql) : sql(sql) {
		int ret = sqlite3_prepare_v2(p->sql, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
		if (ret != SQLITE_OK) {
			throw Error::Database::formatted("error {} during SQL prepared statement creation: '{}': {}",
											 ret, sql.c_str(), sqlite3_errstr(ret));
		}

		if (!stmt)
			throw Error::Database::formatted("Unknown error during prepared statement creation");
	}

	~Statement() {
		std::ignore = sqlite3_finalize(stmt);
		stmt = nullptr;
	}

	void step() {
		std::ignore = db::stepWrapper(stmt, sql.c_str());
	}

	RowMapper rows() {
		spdlog::trace("running: {}", sql);
		while (true) {
			int ret = sqlite3_step(stmt);

			if (ret == SQLITE_BUSY)
				continue;
			else if (ret == SQLITE_DONE)
				break;
			else if (ret == SQLITE_ROW) {
				auto rw = RowMapper(sql, stmt, true);
				rw.refHolder = shared_from_this();
				return rw;
			} else {
				throw Error::Database::formatted("error {} during SQL: '{}': {}", ret, sql.c_str(),
												 sqlite3_errstr(ret));
			}
		}

		auto rw = RowMapper(sql, stmt, false);
		rw.refHolder = shared_from_this();
		return rw;
	}

	template <typename A>
	std::shared_ptr<Statement> bind(const char* name, A data) {
		int index = sqlite3_bind_parameter_index(stmt, name);
		if (index == 0) {
			throw Error::Database::formatted("tried to look up parameter '{}', but there's no such parameter", name);
		}

		int ret = bindGeneral(index, data);
		if (ret != SQLITE_OK) {
			throw Error::Database::formatted("tried to bind argument {} for SQL '{}', but failed", index, sql);
		}

		return shared_from_this();
	}

private:
	std::string sql;
	sqlite3_stmt* stmt = nullptr;

	int bindGeneral(int index, int data) { return sqlite3_bind_int(stmt, index, data); }

	int bindGeneral(int index, uint64_t data) {
		return sqlite3_bind_int64(stmt, index, static_cast<sqlite3_int64>(data));
	}

	int bindGeneral(int index, long data) {
		return sqlite3_bind_int64(stmt, index, static_cast<sqlite3_int64>(data));
	}

	int bindGeneral(int index, const std::string& str) {
		return sqlite3_bind_text(stmt, index, str.c_str(), static_cast<int>(str.size()), SQLITE_STATIC);
	}
};

Database::Database(const std::string& fname) {
	p = std::make_shared<DatabasePrivate>(fname);

	sql("create table if not exists cache (id integer primary key, url text, data text, created datetime)");
	sql("create index if not exists cache_url on cache (url)");
	sql("create index if not exists cache_created on cache (created)");
}

void Database::sql(const std::string& expr) {
	Statement(p, expr).step();
}

std::optional<std::string> Database::getCacheForURL(const std::string& url) {
	auto rows = Statement::create(p, "select data from cache where url=:url")
		->bind(":url", url)
		->rows();

	while (rows.next()) {
		return rows.get<std::string>("data");
	}

	return {};
}

void Database::putCacheForURL(const std::string& url, const std::string& data) {
	Statement::create(p, "delete from cache where url=:url")
		->bind(":url", url)
		->step();

	Statement::create(p, "insert into cache (url, data, created) values (:url, :data, datetime('now'))")
		->bind(":url", url)
		->bind(":data", data)
		->step();
}

uint64_t Database::putLobstersPost(
	uint64_t parentId,
	const std::string& shortId,
	const std::string& author,
	const std::string& title,
	const std::string& body,
	uint64_t timestamp)
{
	auto rows = Statement::create(p, "select id from lobsters_posts where shortId=:shortId")
		->bind(":shortId", shortId)
		->rows();

	int foundId = -1;
	while (rows.next()) {
		foundId = rows.get<int>("id");
		break;
	}

	if (foundId == -1) {
		Statement::create(p, "insert into lobsters_posts (parentId, author, title, body, "
						     "created, added, shortId) values (:pid, :aut, :tit, :bod, :cr, "
						     "datetime('now'), :sid)")
			->bind(":pid", parentId)
			->bind(":aut", author)
			->bind(":tit", title)
			->bind(":bod", body)
			->bind(":cr", timestamp)
			->bind(":sid", shortId)
			->step();

		return sqlite3_last_insert_rowid(p->sql);
	} else {
		Statement::create(p, "update lobsters_posts set body=:bod where id=:id")
			->bind(":bod", body)
			->bind(":id", foundId)
			->step();

		return foundId;
	}
}

uint64_t Database::putLobstersPostInStaging(
	const std::string& parentId,
	const std::string& shortId,
	const std::string& author,
	const std::string& title,
	const std::string& body,
	uint64_t timestamp)
{
	Statement::create(p, "insert into lobsters_staging (parentShortId, author, title, body, "
						 "created, shortId) values (:pid, :aut, :tit, :bod, :cr, "
						 ":sid)")
		->bind(":pid", parentId)
		->bind(":aut", author)
		->bind(":tit", title)
		->bind(":bod", body)
		->bind(":cr", timestamp)
		->bind(":sid", shortId)
		->step();

	return sqlite3_last_insert_rowid(p->sql);
}

void Database::purgeCache() {
	auto rs = Statement::create(p, "select count(*) as cnt from cache")->rows();
	while (rs.next()) {
		auto count = rs.get<uint64_t>("cnt");
		sql("delete from cache");
		spdlog::info("Purged {} item(s).", count);
		return;
	}

	spdlog::info("Purged 0 item(s).");
}

std::pair<uint64_t, uint64_t> Database::getMinMaxForTable(const std::string& table) {
	auto rs = Statement::create(p, "select min(id) as min, max(id) as max from " + table)->rows();
	return { rs.get<uint64_t>("min"), rs.get<uint64_t>("max") };
}

std::pair<uint64_t, uint64_t> Database::getMinMaxAfterTSForTable(uint64_t ts, const std::string& table) {
	auto rs = Statement::create(p, "select min(id) as min, max(id) as max from " + table + " where created > :cr")
		->bind(":cr", ts)
		->rows();
	return { rs.get<uint64_t>("min"), rs.get<uint64_t>("max") };
}

std::optional<uint64_t> Database::getArticleCountForTable(const std::string& table) {
	auto rs = Statement::create(p, "select count(*) as cnt from " + table)->rows();
	return { rs.get<uint64_t>("cnt") };
}

std::vector<RawLobstersPost> Database::getLobstersPostAfterTimestamp(uint64_t ts, bool readBody) {
	auto sql = fmt::format("select shortId,author,title,created,id,parentId{} from lobsters_posts where created >= :cr", readBody ? ",body" : "");
	auto rs = Statement::create(p, sql)->bind(":cr", ts)->rows();
	std::vector<RawLobstersPost> vec;

	while (rs.next()) {
		auto parentId = rs.get<uint64_t>("parentId");
		auto id = rs.get<uint64_t>("id");
		auto shortId = rs.get<std::string>("shortId");
		auto author = rs.get<std::string>("author");
		auto title = rs.get<std::string>("title");
		auto created = rs.get<uint64_t>("created");

		std::string body;
		if (readBody) {
			body = rs.get<std::string>("body");
		}

		vec.emplace_back(RawLobstersPost {
			.parentId = parentId,
			.id = id,
			.shortId = shortId,
			.author = author,
			.body = body,
			.title = title,
			.timestamp = created,
			.bodyRequested = readBody
		});
	}

	return vec;
}

std::optional<RawLobstersPost> Database::getLobstersPost(uint64_t id, bool readBody) {
	auto sql = fmt::format("select shortId,author,title,created,parentId{} from lobsters_posts where id=:id", readBody ? ",body" : "");
	auto rs = Statement::create(p, sql)->bind(":id", id)->rows();

	while (rs.next()) {
		auto parentId = rs.get<uint64_t>("parentId");
		auto shortId = rs.get<std::string>("shortId");
		auto author = rs.get<std::string>("author");
		auto title = rs.get<std::string>("title");
		auto created = rs.get<uint64_t>("created");

		std::string body;
		if (readBody) {
			body = rs.get<std::string>("body");
		}

		return RawLobstersPost {
			.parentId = parentId,
			.id = id,
			.shortId = shortId,
			.author = author,
			.body = body,
			.title = title,
			.timestamp = created,
			.bodyRequested = readBody
		};
	}

	return std::nullopt;
}

std::optional<uint64_t> Database::getLobstersIdByShortId(const std::string& shortId) {
	auto sql = fmt::format("select id from lobsters_posts where shortId=:sid");
	auto rs = Statement::create(p, sql)->bind(":sid", shortId)->rows();

	while (rs.next()) {
		return rs.get<uint64_t>("id");
	}

	return std::nullopt;
}

void Database::eachStagingPost(std::function<void (RawLobstersPost&)> closure) {
	auto sql = fmt::format("select id,parentShortId,shortId,title,author,created,body from lobsters_staging order by created asc");
	auto rs = Statement::create(p, sql)->rows();

	while (rs.next()) {
		auto id = rs.get<uint64_t>("id");
		auto parentShortId = rs.get<std::string>("parentShortId");
		auto shortId = rs.get<std::string>("shortId");
		auto author = rs.get<std::string>("author");
		auto title = rs.get<std::string>("title");
		auto body = rs.get<std::string>("body");
		auto created = rs.get<uint64_t>("created");

		auto raw = RawLobstersPost {
			.parentId = 0,
			.id = id,
			.shortId = shortId,
			.parentShortId = parentShortId,
			.author = author,
			.body = body,
			.title = title,
			.timestamp = created,
			.bodyRequested = true
		};

		closure(raw);
	}
}

void Database::clearStaging() {
	sql("delete from lobsters_staging");
}

}
