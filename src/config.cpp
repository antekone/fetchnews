#include <memory>
#include <initializer_list>

#include <toml.hpp>

#include "config.h"
#include "errors.h"

using std::string;

std::shared_ptr<Config> Config::singleton;

std::shared_ptr<Config> Config::get(const string& path) {
	try {
		if (path.empty()) {
			throw Error::Config("configuration file not specified!");
		}

		const auto conf = toml::parse_file(path);
		std::optional<LobstersConfig> lobsters = {};

		const auto general = conf["general"];
		auto bind = general["bind"].value<string>();
		auto cache = general["cache"].value<string>();

		if (!bind) throw Error::Config("missing general/bind");
		if (!cache) throw Error::Config("missing general/cache");

		if (conf.contains("lobsters")) {
			const auto tbl = conf["lobsters"];

			auto user = tbl["username"].value<string>();
			auto pass = tbl["password"].value<string>();
			auto update = tbl["update"].value_or<int>(5);

			if ((!user && pass) || (user && !pass))
				throw Error::Config("either provide both lobsters/user and lobsters/pass, or neither");

			lobsters = LobstersConfig {
				.username = user,
				.password = pass,
				.update = update
			};
		}

		auto config = std::make_shared<Config>(GeneralConfig {
			.bind = *bind, .cache = *cache
		}, lobsters);

		singleton = config;
		return config;
	} catch (const toml::parse_error& e) {
		throw Error::Config(e.what());
	}
}