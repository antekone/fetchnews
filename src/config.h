#pragma once

#include <optional>
#include <string>
#include <memory>

struct GeneralConfig {
	std::string bind;
	std::string cache;
};

struct LobstersConfig {
	std::optional<std::string> username;
	std::optional<std::string> password;
	int update = 0;
};

class Config {
public:
	Config(GeneralConfig g, std::optional<LobstersConfig> lobsters)
		: general(std::move(g)), lobsters(std::move(lobsters)) {}

	static std::shared_ptr<Config> get(const std::string& path);
	static std::shared_ptr<Config> get() { return singleton; }

	GeneralConfig general;
	std::optional<LobstersConfig> lobsters;

private:
	static std::shared_ptr<Config> singleton;
};
