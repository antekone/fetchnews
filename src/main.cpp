#include <iostream>
#include <optional>
#include <fstream>
#include <vector>
#include <thread>
#include <sstream>

#include <boost/asio.hpp>
#include <cxxopts.hpp>
#include <spdlog/spdlog.h>

#include "errors.h"
#include "http.h"
#include "config.h"
#include "nntp.h"
#include "engines/engine.h"
#include "engines/lobsters.h"

static const char* ARG_HELP = "help";
static const char* ARG_CONFIG = "config";
static const char* ARG_VERBOSE = "verbose";
static const char* ARG_CRAWL_TICK = "crawler-tick";
static const char* ARG_CRAWL_MODE = "crawl-mode";
static const char* ARG_PURGE_CACHE = "purge-cache";

struct Context {
	std::optional<std::string> configFile;
	std::optional<std::string> crawlTick;
	bool crawlMode = false;
	bool forceQuit = false;
	bool verbose = false;
	bool purgeCache = false;
	std::shared_ptr<boost::asio::io_context> io;
};

template <typename A>
std::optional<A> asOptional(const cxxopts::ParseResult& r, const char* name) {
	try {
		if (r[name].count() > 0) {
			return r[name].as<A>();
		} else {
			return std::nullopt;
		}
	} catch (const std::bad_cast& e) {
		throw Error::Argument::formatted("Unexpected type of value for option `{}`.", name);
	}
}

template <typename A>
std::vector<A> asArray(const cxxopts::ParseResult& r, const char* name) {
	auto arr = asOptional<std::vector<A>>(r, name);
	if (!arr)
		return std::vector<A>();
	else {
		return *arr;
	}
}

std::string defaultConfigPath() {
	const auto* ptr = getenv("FETCHNEWS_CONFIG");
	if (!ptr)
		return "";
	else
		return ptr;
}

Context createContext(int argc, char** argv) {
	using std::vector;
	using std::string;

	cxxopts::Options options("fetchnews", "NNTP server proxy for several websites");
	options
		.add_options("General")
			(fmt::format("c,{}", ARG_CONFIG), "Use this configuration file (overrides $FETCHNEWS_CONFIG environment variable)", cxxopts::value<string>())
			(ARG_CRAWL_TICK, "Invoke one crawler tick, provide engine name in the argument or `list` to get a list of available crawlers", cxxopts::value<string>()->default_value("list"))
			(ARG_CRAWL_MODE, "Run crawlers once (best to run this periodically from cron)")
			(ARG_PURGE_CACHE, "Remove web cache from the database")
			(fmt::format("h,{}", ARG_HELP), "This help screen")
			(fmt::format("v,{}", ARG_VERBOSE), "Use verbose logging")
	;

	try {
		auto result = options.parse(argc, argv);
		if (result.count(ARG_HELP) > 0) {
			fmt::print("{}", options.help());
			return Context { .forceQuit = true };
		}

		return Context {
			.configFile = asOptional<string>(result, ARG_CONFIG),
			.crawlTick = asOptional<string>(result, ARG_CRAWL_TICK),
			.crawlMode = asOptional<bool>(result, ARG_CRAWL_MODE).has_value(),
			.verbose = asOptional<bool>(result, ARG_VERBOSE).has_value(),
			.purgeCache = asOptional<bool>(result, ARG_PURGE_CACHE).has_value(),
			.io = std::make_shared<boost::asio::io_context>()
		};
	} catch (cxxopts::OptionException& e) {
		throw Error::Argument(e.what());
	}
}

EngineList populateEngines(std::shared_ptr<Config>& conf, std::shared_ptr<db::Database>& db) {
	auto elist = EngineList();
	elist.add(LobstersEngine::create(conf, db, http::Client::create()));
	elist.purge();
	return elist;
}

void crawlerTest(std::shared_ptr<Config>& conf, const std::string& crawlerName) {
	auto db = db::Database::create(conf->general.cache);
	auto elist = populateEngines(conf, db);

	if (crawlerName == "list") {
		spdlog::info("Available engines:");

		for (const auto& name : elist.getNames()) {
			spdlog::info("- '{}'", name);
		}

		return;
	}

	elist.filter(crawlerName);

	if (elist.size() == 0) {
		spdlog::error("Requested engine doesn't exist or is disabled: '{}'", crawlerName);
	}

	elist.runTick();
}

void runCrawlers(std::shared_ptr<Config>& conf) {
	auto db = db::Database::create(conf->general.cache);
	auto elist = populateEngines(conf, db);
	spdlog::trace("Running all crawlers...");
	elist.runTick();
}

int run(const Context& c) {
	auto conf = Config::get(c.configFile.value_or(defaultConfigPath()));
	boost::asio::steady_timer t(*c.io);

	if (c.purgeCache) {
		auto db = db::Database::create(conf->general.cache);
		db->purgeCache();
		return 0;
	}

	if (c.crawlTick) {
		t.async_wait([&conf, &c] (const auto& err) {
			crawlerTest(conf, *c.crawlTick);
		});

		c.io->run();
		return 0;
	}

	if (c.crawlMode) {
		t.async_wait([&conf, &c] (const auto& err) {
			runCrawlers(conf);
		});

		c.io->run();
		return 0;
	}

	spdlog::info("Starting listener on: {}", conf->general.bind);
	auto db = db::Database::create(conf->general.cache);
	auto elist = populateEngines(conf, db);
	elist.purge();
	auto nntp = NNTPServer::create(*c.io, db, elist);

	spdlog::trace("Entering I/O loop");
	c.io->run();
	spdlog::trace("I/O context finished");

	return 0;
}

int main(int argc, char** argv) {
	try {
		auto ctx = createContext(argc, argv);
		if (ctx.forceQuit) {
			return 0;
		}

		if (ctx.verbose) {
			spdlog::default_logger()->set_level(spdlog::level::trace);
		}

		return run(ctx);
	} catch (const Error::Argument& e) {
		spdlog::error("Error in command line: {}", e.what());
		spdlog::error("Use `--help` to get help about usage.");
		return 1;
	} catch (const Error::IO& e) {
		spdlog::error("I/O error, can't continue: {}", e.what());
		return 1;
	} catch (const Error::Config& e) {
		spdlog::error("Configuration error: {}", e.what());
		return 1;
	} catch (const Error::Database& e) {
		spdlog::error("Database internal error: {}", e.what());
		return 1;
	} catch (const Error::Internal& e) {
		spdlog::error("Internal error (a bug!): {}", e.what());
		return 1;
	}

	return 0;
}
