#include <lexbor/html/parser.h>
#include <lexbor/dom/interfaces/element.h>

#include "../errors.h"
#include "wrappers.h"

namespace lexbor {

struct HTMLDocumentPrivate {
	lxb_html_document *doc;

	HTMLDocumentPrivate() {
		doc = ::lxb_html_document_create();
	}

	~HTMLDocumentPrivate() {
		::lxb_html_document_destroy(doc);
	}
};

HTMLDocument::HTMLDocument() {
	p = std::make_shared<HTMLDocumentPrivate>();
}

void HTMLDocument::parse(const std::string& buf) {
	auto ret = ::lxb_html_document_parse(p->doc, reinterpret_cast<const lxb_char_t*>(buf.c_str()), buf.size());
	if (ret != LXB_STATUS_OK) {
		throw Error::Parse::formatted("HTML parser error: {}", ret);
	}
}

}