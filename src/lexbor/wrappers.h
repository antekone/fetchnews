#pragma once
#include <memory>

namespace lexbor {

struct HTMLDocumentPrivate;

class HTMLDocument final {
public:
	HTMLDocument();
	virtual ~HTMLDocument() = default;

	void parse(const std::string& buf);

private:
	std::shared_ptr<HTMLDocumentPrivate> p;
};

}