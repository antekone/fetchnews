#include <regex>
#include <thread>
#include <chrono>

#include <spdlog/spdlog.h>
#include <nlohmann/json.hpp>
#include <pugixml.hpp>
#include <date/tz.h>
#include <gsl/gsl>

#include "lobsters.h"
#include "../errors.h"
#include "../helpers.h"

struct Comment {
	static inline Creator<Comment> create;

	uint64_t timestamp;
	std::string createdAt;
	std::string author;
	std::string body;
	std::string shortId;
	int indent;
	std::vector<std::shared_ptr<Comment>> comments;

	// runtime fields
	std::string parentShortId;
	std::string title;

};

using CommentRef = std::shared_ptr<Comment>;

LobstersEngine::LobstersEngine(
	std::shared_ptr<Config>& conf,
	std::shared_ptr<db::Database> db,
	std::shared_ptr<http::Client> web
) : Engine("lobsters", std::move(db), std::move(web)), conf(conf) {

}

void LobstersEngine::run() {
	if (!enabled())
		return;

	storyURLsToProcess.clear();

	initDB();
	db->clearStaging();

	findStoryURLsFromJSONFeed("https://lobste.rs/newest.json");
	findStoryURLsFromRSSFeed("https://lobste.rs/comments.rss");
	findStoryURLsFromRSSFeed("https://lobste.rs/rss");
	findStoryURLsFromRSSFeed("https://lobste.rs/top.rss");

	extractContent();
	applyStagingPosts();
	db->clearStaging();
}

void LobstersEngine::initDB() {
	// `created` is UTC time as milliseconds from epoch

	db->sql(R"(
		create table if not exists lobsters_posts (
			id integer primary key,
			parentId integer,
			shortId text,
			author text,
			title text,
			body text,
			created datetime,
			added datetime
		)
	)");

	db->sql(R"(
		create table if not exists lobsters_staging (
			id integer primary key,
			parentShortId text,
			shortId text,
			author text,
			title text,
			body text,
			created datetime
		)
	)");

	db->sql("create index if not exists lobsters_posts_shortId on lobsters_posts (shortId)");
	db->sql("create index if not exists lobsters_posts_created on lobsters_posts (created)");
	db->sql("create index if not exists lobsters_posts_parentId on lobsters_posts (parentId)");

	db->sql("create index if not exists lobsters_staging_parentShortId on lobsters_staging (parentShortId)");
	db->sql("create index if not exists lobsters_staging_created on lobsters_staging (created)");
}

void LobstersEngine::findStoryURLsFromJSONFeed(const std::string& url1) {
	using nlohmann::json;

	auto result = web->get(url1, db);
	if (!result->ok())
		throw Error::Net::formatted("can't access '{}'", url1);

	try {
		auto object = json::parse(result->body);
		if (!object.is_array())
			throw Error::Parse::formatted("Expected JSON array: {}", url1);

		for (const auto& elem : object.get<json::array_t>()) {
			auto shortUrl = elem["short_id_url"].get<json::string_t>();
			addUniqueStoryURL(shortUrl);
		}
	} catch (const json::parse_error& e) {
		throw Error::Parse::formatted("Ill-formed JSON served from '{}'", url1);
	} catch (const json::type_error& e) {
		throw Error::Parse::formatted("Ill-formed JSON served from '{}'", url1);
	}
}

void LobstersEngine::findStoryURLsFromRSSFeed(const std::string& url) {
	auto result = web->get(url, db);
	if (!result->ok())
		throw Error::Net::formatted("can't access '{}'", url);

	pugi::xml_document doc;
	auto xmlResult = doc.load_string(result->body.c_str());
	if (!xmlResult) {
		throw Error::Parse::formatted("Ill-formed XML served from '{}'", url);
	}

	static const std::regex sanitizeUrl { "(https://lobste.rs/s/.+?)/.*" };
	for (const auto& node : doc.select_nodes("/rss/channel/item/comments")) {
		auto link = node.node();
		std::string extractedUrl = link.text().get();

		std::smatch match;
		if (std::regex_match(extractedUrl, match, sanitizeUrl) && match.size() > 1) {
			addUniqueStoryURL(match[1].str());
		}
	}
}

void LobstersEngine::applyStagingPosts() {
	db->sql("begin transaction");
	auto guard = gsl::finally([this] () {
		db->sql("commit");
	});

	db->eachStagingPost([this] (const auto& post) {
		uint64_t parentId = 0;
		if (!post.parentShortId.empty()) {
			parentId = *db->getLobstersIdByShortId(post.parentShortId);
		}

		db->putLobstersPost(parentId,
							post.shortId,
							post.author,
							post.title,
							post.body,
							post.timestamp);
	});
}

void LobstersEngine::extractContent() {
	for (const auto& storyURL : storyURLsToProcess) {
		try {
			processStoryURL(storyURL);
		} catch (const Error::Net& e) {
			spdlog::error("Network error: {}, skipping this page.", e.what());
			spdlog::error("This happened when trying to get URL: {}", storyURL);
		} catch (const Error::Parse& e) {
			spdlog::error("Ill-formed data received: {}, skipping this page.", e.what());
			spdlog::error("This happened when trying to get URL: {}", storyURL);
		}
	}
}

void LobstersEngine::addUniqueStoryURL(const std::string& url) {
	if (std::none_of(
		storyURLsToProcess.begin(),
		storyURLsToProcess.end(),
		[&url] (const auto& existingURL) { return existingURL == url; }
	)) {
		storyURLsToProcess.emplace_back(url);
	}
}

uint64_t parseDate(const std::string& in) {
	//	2021-08-30T07:46:56.000-05:00

	static const std::regex pattern { R"((.*?)\.\d+(.*))" };
	const std::string out = std::regex_replace(in, pattern, "$1$2");

	std::istringstream istr { out };
	date::utc_time<std::chrono::milliseconds> utcTime;
	istr >> date::parse("%FT%T%z", utcTime);

	auto dur = utcTime.time_since_epoch();
	return dur.count();
}

void dump(std::vector<CommentRef>& vec, int indent) {
	std::string spaces;
	for (int i = 0; i < indent*4; i++) {
		spaces += " ";
	}

	for (const auto& el : vec) {
		spdlog::trace("{}- {} [{}]", spaces, el->author, el->shortId);
		dump(el->comments, indent + 1);
	}
}

void LobstersEngine::processStoryURL(const std::string& url) {
	using nlohmann::json;

	const auto jsonURL = fmt::format("{}.json", url);
	auto result = web->get(jsonURL, db);
	if (!result->ok()) {
		spdlog::error("failed to extract story information from url: '{}'", jsonURL);
		return;
	}

	auto object = json::parse(result->body);
	if (!object.is_object() || !object.contains("comments") || !object["comments"].is_array())
		throw Error::Parse::formatted("Ill-formed JSON received from {}", jsonURL);

	const auto storyTimestamp = parseDate(object["created_at"].get<json::string_t>());
	const auto storyAuthor = object["submitter_user"]["username"].get<json::string_t>();
	const auto storyBody = strip(fmt::format("{}\n\n{}",
		object["url"].get<json::string_t>(),
		object["description"].get<json::string_t>()));
	const auto storyShortId = object["short_id"].get<json::string_t>();
	const auto storyTitle = object["title"].get<json::string_t>();

	db->sql("begin transaction");

	std::vector<CommentRef> commentTree;
	std::map<int, CommentRef> commentIndentCache;

	std::ignore = db->putLobstersPostInStaging("", storyShortId, storyAuthor, storyTitle, storyBody, storyTimestamp);

	for (const auto& comment : object["comments"].get<json::array_t>()) {
		const auto timestamp = parseDate(comment["created_at"].get<json::string_t>());
		const int indentLevel = comment["indent_level"].get<json::number_integer_t>();
		const auto body = comment["comment"].get<json::string_t>();
		const auto author = comment["commenting_user"]["username"].get<json::string_t>();
		const auto shortId = comment["short_id"].get<json::string_t>();

		auto c = Comment::create();
		c->timestamp = timestamp;
		c->author = author;
		c->body = body;
		c->shortId = shortId;
		c->indent = indentLevel;
		c->createdAt = comment["created_at"].get<json::string_t>();

		commentTree.push_back(c);
		commentIndentCache[indentLevel] = c;

		if (indentLevel > 1) {
			auto parent = commentIndentCache[indentLevel - 1];
			c->parentShortId = parent->shortId;
			c->title = parent->title;
		} else {
			c->parentShortId = storyShortId;
			c->title = storyTitle;
		}
	}

	std::sort(commentTree.begin(), commentTree.end(), [] (const auto& a, const auto& b) {
		return b->timestamp > a->timestamp;
	});

	for (const auto& post : commentTree) {
		std::ignore =
			db->putLobstersPostInStaging(post->parentShortId,
										 post->shortId,
										 post->author,
										 post->title,
										 post->body,
										 post->timestamp);
	}

	db->sql("commit");
}

void LobstersEngine::populateNewsgroupNames(std::vector<std::string>& arr) {
	arr.emplace_back("lobsters.all");
}

std::pair<uint64_t, uint64_t> LobstersEngine::getArticleIdRange(const std::string& name) {
	if (name == "lobsters.all") {
		return db->getMinMaxForTable("lobsters_posts");
	} else {
		return { 0, 0 };
	}
}

std::pair<uint64_t, uint64_t> LobstersEngine::getArticleIdRangeAfterTimestamp(uint64_t ts, const std::string& name) {
	if (name == "lobsters.all") {
		return db->getMinMaxAfterTSForTable(ts, "lobsters_posts");
	} else {
		return { 0, 0 };
	}
}

bool LobstersEngine::postingEnabled(const std::string& name) {
	return false;
}

bool LobstersEngine::acceptsNewsgroupName(const std::string& groupName) {
	return groupName.starts_with("lobsters.");
}

std::optional<uint64_t> LobstersEngine::getNumberOfArticles(const std::string& newsgroupName) {
	if (newsgroupName == "lobsters.all") {
		return db->getArticleCountForTable("lobsters_posts");
	} else {
		return 0;
	}
}

std::optional<ArticleList> LobstersEngine::getArticleAfterTimestamp(uint64_t ts, bool readBody) {
	auto lst = ArticleList {};
	auto posts = db->getLobstersPostAfterTimestamp(ts, readBody);

	for (const auto& post : posts) {
		auto article = LobstersArticle::create(post.title,
											   post.body,
											   post.author,
											   post.timestamp,
											   post.shortId,
											   post.parentId,
											   post.id);
		lst.add(article);
	}

	return lst;
}

std::optional<ArticleList> LobstersEngine::getArticleRange(uint64_t from, uint64_t to, bool readBody) {
	auto lst = ArticleList {};

	if (from > to) {
		return std::nullopt;
	}

	for (uint64_t id = from; id <= to; id++) {
		const auto post = db->getLobstersPost(id, readBody);
		if (!post) {
			spdlog::error("Requested lobste.rs post #{}, but there was no such post in the DB", id);
			continue;
		}

		auto article = LobstersArticle::create(
			post->title,
			post->body,
			post->author,
			post->timestamp,
			post->shortId,
			post->parentId,
			id);

		lst.add(article);
	}

	return lst;
}
