#pragma once

#include <vector>
#include <string>
#include <memory>

#include "engine.h"
#include "../config.h"
#include "../helpers.h"

struct Comment;

class LobstersEngine;

class LobstersArticle final : public Article {
public:
	static inline Creator<LobstersArticle> create;

	LobstersArticle(
		std::string title,
		std::string body,
		std::string author,
		uint64_t created,
		const std::string& shortId,
		uint64_t parentId,
		uint64_t id
	) : Article() {
		subject = std::move(title);

		if (parentId > 0) {
			subject = "Re: " + subject;
		}

		from = std::move(author);
		timestamp = created;
		messageId = fmt::format("<{}fn@{}>", shortId, "lobste.rs");


		lines = std::count_if(body.begin(), body.end(), [] (const auto ch) { return ch == '\n'; });
		bytes = body.size();

		this->parentId = parentId;
		this->id = id;
		this->body = std::move(body);

		date::sys_time<std::chrono::milliseconds> ms;
	}

protected:
};

class LobstersEngine final : public Engine {
public:
	explicit LobstersEngine(std::shared_ptr<Config>& conf, std::shared_ptr<db::Database> db, std::shared_ptr<http::Client> web);

	static inline Creator<LobstersEngine> create;

	void run() override;
	void populateNewsgroupNames(std::vector<std::string>& arr) override;
	std::pair<uint64_t, uint64_t> getArticleIdRange(const std::string& name) override;
	std::pair<uint64_t, uint64_t> getArticleIdRangeAfterTimestamp(uint64_t ts, const std::string& name) override;
	std::optional<uint64_t> getNumberOfArticles(const std::string& name) override;
	std::optional<ArticleList> getArticleRange(uint64_t from, uint64_t to, bool readBody) override;
	std::optional<ArticleList> getArticleAfterTimestamp(uint64_t ts, bool readBody) override;
	bool postingEnabled(const std::string& name) override;
	bool acceptsNewsgroupName(const std::string& groupName) override;

	[[nodiscard]] bool enabled() const override { return static_cast<bool>(conf->lobsters); }

private:
	std::shared_ptr<Config> conf;
	std::vector<std::string> storyURLsToProcess;

	void initDB();
	void findStoryURLsFromJSONFeed(const std::string& url1);
	void findStoryURLsFromRSSFeed(const std::string& url);
	void extractContent();
	void applyStagingPosts();
	void addUniqueStoryURL(const std::string& existingURL);
	void processStoryURL(const std::string& url);
	void recursivePersist(const std::vector<std::shared_ptr<Comment>>& vec, const std::string& parentTitle, std::string parentShortId, std::vector<db::RawLobstersPost>& flat);
};
