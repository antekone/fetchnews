#pragma once

#include <string>
#include <memory>
#include <vector>
#include <optional>
#include <algorithm>

#include "../http.h"
#include "../db.h"

class ArticleList;

class Article {
public:
	[[nodiscard]] uint64_t getId() const { return id; }
	[[nodiscard]] uint64_t getParentId() const { return parentId; }
	[[nodiscard]] uint64_t getBytes() const { return bytes; }
	[[nodiscard]] uint64_t getLines() const { return lines; }
	[[nodiscard]] uint64_t getTimestamp() const { return timestamp; }

	[[nodiscard]] const std::string& getSubject() const { return subject; }
	[[nodiscard]] const std::string& getFrom() const { return from; }
	[[nodiscard]] const std::string& getMessageId() const { return messageId; }
	[[nodiscard]] const std::string& getBody() const { return body; }

protected:
	uint64_t bytes = 0;
	uint64_t lines = 0;
	uint64_t timestamp = 0;
	uint64_t id = 0;
	uint64_t parentId = 0;
	std::string subject;
	std::string from;
	std::string messageId;
	std::string body;
	std::vector<std::string> references;
};

using ArticleRef = std::shared_ptr<Article>;

class Engine {
public:
	explicit Engine(std::string name, std::shared_ptr<db::Database> db, std::shared_ptr<http::Client> web)
		: name(std::move(name)), db(std::move(db)), web(std::move(web))
	{
	}

	virtual ~Engine() = default;

	virtual void run() = 0;
	virtual void populateNewsgroupNames(std::vector<std::string>& arr) = 0;
	virtual bool acceptsNewsgroupName(const std::string& groupName) = 0;
	virtual std::pair<uint64_t, uint64_t> getArticleIdRange(const std::string& newsgroupName) = 0;
	virtual std::pair<uint64_t, uint64_t> getArticleIdRangeAfterTimestamp(uint64_t ts, const std::string& newsgroupName) = 0;
	virtual std::optional<ArticleList> getArticleRange(uint64_t from, uint64_t to, bool readBody) = 0;
	virtual std::optional<ArticleList> getArticleAfterTimestamp(uint64_t ts, bool readBody) = 0;
	virtual std::optional<uint64_t> getNumberOfArticles(const std::string& newsgroupName) = 0;
	virtual bool postingEnabled(const std::string& newsgroupName) = 0;

	[[nodiscard]] virtual bool enabled() const = 0;
	[[nodiscard]] bool disabled() const { return !enabled(); }
	[[nodiscard]] const std::string& getName() const { return name; }

protected:
	std::string name;
	std::shared_ptr<http::Client> web;
	std::shared_ptr<db::Database> db;
};

using EngineRef = std::shared_ptr<Engine>;

class ArticleList {
public:
	template <typename A>
	void add(std::shared_ptr<A> article) {
		articles.emplace_back(std::move(article));
	}

	std::vector<std::shared_ptr<Article>>& getArticles() { return articles; }
	[[nodiscard]] size_t size() const { return articles.size(); }

private:
	std::vector<std::shared_ptr<Article>> articles;

};

class EngineList {
public:
	template <typename A>
	void add(std::shared_ptr<A> engine) {
		engines.emplace_back(std::move(engine));
	}

	void purge() {
		engines.erase(std::remove_if(engines.begin(), engines.end(), [] (const auto& e) {
			return !e->enabled();
		}), engines.end());
	}

	std::optional<std::shared_ptr<Engine>> findByName(const std::string& name) {
		auto result = std::find_if(engines.begin(), engines.end(), [&name] (auto& engine) {
			return engine->getName() == name;
		});

		if (result == engines.end())
			return {};
		else {
			return *result;
		}
	}

	void runTick() {
		for (const auto& e : engines) {
			e->run();
		}
	}

	void filter(const std::string& name) {
		if (!name.empty()) {
			engines.erase(std::remove_if(engines.begin(), engines.end(), [&name](const auto &engine) {
				return (engine->disabled() || engine->getName() != name);
			}), engines.end());
		}
	}

	std::vector<std::string> getNames() {
		std::vector<std::string> arr;

		for (const auto& e : engines) {
			arr.push_back(e->getName());
		}

		return arr;
	}

	std::vector<std::string> compileList() {
		std::vector<std::string> arr;

		for (const auto& e : engines) {
			e->populateNewsgroupNames(arr);
		}

		return arr;
	}

	std::vector<std::shared_ptr<Engine>>& getEngines() { return engines; }

	[[nodiscard]] size_t size() const { return engines.size(); }

private:
	std::vector<std::shared_ptr<Engine>> engines;
};
