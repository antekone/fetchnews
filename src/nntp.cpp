#include <regex>
#include <sstream>

#include <boost/bind/bind.hpp>
#include <spdlog/spdlog.h>
#include <gsl/gsl>
#include <date/tz.h>

#include "nntp.h"
#include "config.h"
#include "errors.h"
#include "helpers.h"
#include "engines/engine.h"

enum class ArticleRequest {
	head, body, article
};

using namespace boost::asio;

struct NNTPState {
	const std::string banner = "fetchnews NNTP server substitute";
	std::optional<std::string> currentGroup;

	void reset() {
		currentGroup = {};
	}
};

class NNTPConnection : public std::enable_shared_from_this<NNTPConnection> {
public:
	static inline Creator<NNTPConnection> create;
	explicit NNTPConnection(io_context& io, DatabaseRef db, EngineList& engines) : socket(io), db(std::move(db)), engines(engines) {}

	ip::tcp::socket& getSocket() { return socket; }

	void start() {
		streambuf buf;
		writeStatus(200, state.banner);

		try {
			for (;;) {
				const auto bytesRead = read_until(socket, buf, '\n');
				auto begin = buffers_begin(buf.data());
				auto end = begin + (bytesRead - 1);
				std::string line { strip({ begin, end }) };
				buf.consume(bytesRead);

				spdlog::trace(">>> '{}'", line);
				std::istringstream input { line };

				if (!handleRequest(input))
					break;
			}
		} catch (boost::wrapexcept<boost::system::system_error>& ec) {
			spdlog::error("Connection error: {}", ec.what());
		}
	}

	template <typename... Args>
	void writeLine(const std::string& format, Args... args) {
		std::string buf =
			fmt::format(fmt::runtime(format), std::forward<Args>(args)...);
		buf += "\r\n";
		write(socket, buffer(buf));
	}

	template <typename... Args>
	void writeStatus(int code, const std::string& format, Args... args) {
		writeLine("{} " + format, code, std::forward<Args>(args)...);
	}

	bool handleRequest(std::istream& request) {
		std::string command;

		request >> command;
		uppercase(command);

		if (command == "QUIT" || command == "EXIT") {
			writeStatus(200, "Until next time!");
			return false;
		} else if (command == "MODE") {
			return handleMode(request);
		} else if (command == "LIST") {
			return handleList(request);
		} else if (command == "XOVER" || command == "OVER") {
			return handleOverview(request);
		} else if (command == "GROUP") {
			return handleGroup(request);
		} else if (command == "HEAD") {
			return handleArticle(request, ArticleRequest::head);
		} else if (command == "BODY") {
			return handleArticle(request, ArticleRequest::body);
		} else if (command == "ARTICLE") {
			return handleArticle(request, ArticleRequest::article);
		} else if (command == "AUTHINFO") {
			writeStatus(281, "Everyone is welcome");
			return true;
		} else if (command == "NEWGROUPS") {
			return handleNewgroups(request);
		}

		writeStatus(500, "Unknown command");
		return true;
	}

	bool handleMode(std::istream& request) {
		std::string mode;
		request >> mode;
		uppercase(mode);

		if (mode == "READER") {
			state.reset();
			writeStatus(200, state.banner);
		} else {
			writeStatus(500, "Unknown mode requested");
		}

		return true;
	}

	bool handleGroupWithEngine(EngineRef& e, const std::string& group) {
		const auto [low, high] = e->getArticleIdRange(group);
		const auto count = e->getNumberOfArticles(group);

		if (!count || *count == 0) {
			writeStatus(211, "0 0 0 {}", group);
		} else {
			writeStatus(211, "{} {} {} {}", *count, low, high, group);
		}

		state.currentGroup = group;
		return true;
	}

	bool handleNewgroups(std::istream& request) {
		std::string date, time, tz;
		request >> date >> time >> tz;

		writeStatus(231, "New newsgroups follow");
		auto guard = gsl::finally([this] () {
			writeLine(".");
		});

		if (date.empty() || time.empty() || tz.empty()) {
			return true;
		}

		static const std::regex numbers { R"((\d{2})(\d{2})(\d{2}))" };
		std::smatch dateMatch, timeMatch;

		if (!std::regex_match(date, dateMatch, numbers) && dateMatch.size() != 4) {
			spdlog::error("Wrong date");
			return true;
		}

		if (!std::regex_match(time, timeMatch, numbers) && timeMatch.size() != 4) {
			spdlog::error("Wrong time");
			return true;
		}

		if (tz != "GMT") {
			spdlog::error("Wrong TZ: {}, expected 'GMT'", tz);
			return true;
		}

		auto year = std::strtoull(dateMatch[1].str().c_str(), nullptr, 10);
		auto month = std::strtoull(dateMatch[2].str().c_str(), nullptr, 10);
		auto day = std::strtoull(dateMatch[3].str().c_str(), nullptr, 10);
		auto hour = std::strtoull(timeMatch[1].str().c_str(), nullptr, 10);
		auto minute = std::strtoull(timeMatch[2].str().c_str(), nullptr, 10);
		auto second = std::strtoull(timeMatch[3].str().c_str(), nullptr, 10);

		date::utc_time<chrono::milliseconds> utc_time;
		std::string dateStr = fmt::format("{:04}-{:02}-{:02} {:02}:{:02}:{:02} {}",
										  2000 + year, month, day, hour, minute, second,
										  "GMT");

		std::istringstream istr { dateStr };
		istr >> date::parse("%F %T %Z", utc_time);
		auto timestamp = utc_time.time_since_epoch().count();

		spdlog::trace("newgroups generated date: {} / {}", dateStr, timestamp);

		for (const auto& engine : engines.getEngines()) {
			std::vector<std::string> arr;
			engine->populateNewsgroupNames(arr);

			for (const auto& name: arr) {
				const auto [low, high] = engine->getArticleIdRangeAfterTimestamp(timestamp, name);
				if (low == 0 && high == 0)
					continue;

				bool posting = engine->postingEnabled(name);

				writeLine("{} {} {} {}", name, high, low,
						  posting ? "y" : "n");
			}
		}

		return true;
	}

	bool handleArticle(std::istream& request, ArticleRequest req) {
		uint64_t id;
		request >> id;

		// Fetch by message id is unsupported

		// 220 ok
		// 423 bad article number, article doesn't exist

		if (id == 0) {
			writeStatus(423, "Bad article number");
			return true;
		}

		auto me = getEngineForCurrentGroup();
		if (!me) {
			writeStatus(423, "Internal error -- can't get current engine");
			return true;
		}

		auto e = *me;
		auto articles = e->getArticleRange(id, id, true);
		if (!articles || articles->size() != 1) {
			writeStatus(423, "No such article in current group");
			return true;
		}

		auto post = articles->getArticles()[0];
		auto guard = gsl::finally([this] () {
			writeLine(".");
		});

		switch (req) {
			case ArticleRequest::article: {
				writeStatus(220, "{} {}", id, post->getMessageId());
				auto fullBody = renderArticle(*post, true, false);
				for (const auto &line: fullBody) {
					writeLine("{}", line);
				}
				break;
			}

			case ArticleRequest::head: {
				writeStatus(221, "{} {} head", id, post->getMessageId());
				auto headers = renderArticle(*post, false, false);
				for (const auto &line: headers) {
					writeLine("{}", line);
				}
				break;
			}

			case ArticleRequest::body: {
				writeStatus(222, "{} {} body", id, post->getMessageId());
				auto headers = renderArticle(*post, true, true);
				for (const auto &line: headers) {
					writeLine("{}", line);
				}
				break;
			}
		}

		return true;
	}

	std::vector<std::string> renderArticle(const Article& a, bool renderBody = false, bool skipHeaders = false) {
		std::vector<std::string> out;
		auto date = asDateString(a.getTimestamp());

		if (!skipHeaders) {
			// Probably would be a good idea to escape the username
			out.push_back(fmt::format("Path: fetchnews!.POSTED!not-for-mail", a.getMessageId()));
			out.push_back(fmt::format("From: {} <{}@lobste.rs>", a.getFrom(), a.getFrom()));
			out.push_back(fmt::format("Subject: {}", a.getSubject()));
			out.push_back(fmt::format("Message-ID: {}", a.getMessageId()));
			out.push_back(fmt::format("Lines: {}", a.getLines()));
			out.push_back(fmt::format("Mime-Version: 1.0"));
			out.push_back(fmt::format("Newsgroups: {}", *state.currentGroup));
			out.push_back(fmt::format("Content-Type: text/html; charset=UTF-8; format=flowed"));
			out.push_back(fmt::format("X-fetchnews: true"));
			out.push_back(fmt::format("X-fetchnews-id: {}", a.getId()));
			out.push_back(fmt::format("X-fetchnews-parent-id: {}", a.getParentId()));
			out.push_back(fmt::format("Date: {}", date));
		}

		if (renderBody) {
			if (!skipHeaders) {
				out.emplace_back("");
			}

			auto lines = split(a.getBody(), '\n');
			for (const auto &line: lines) {
				out.push_back(line);
			}
		}

		return out;
	}

	bool handleGroup(std::istream& request) {
		std::string name;
		request >> name;
		strip(name);

		for (auto& engine : engines.getEngines()) {
			if (engine->acceptsNewsgroupName(name)) {
				return handleGroupWithEngine(engine, name);
			}
		}

		writeStatus(411, "Group doesn't exist");
		return true;
	}

	bool handleOverview(std::istream& request) {
		if (!state.currentGroup) {
			writeStatus(412, "Use GROUP to select new group first");
			return true;
		}

		std::string range;
		request >> range;
		strip(range);

		if (range.empty()) {
			writeStatus(420, "No article(s) selected");
			return true;
		}

		const auto decodedRange = decodeRange(range);
		if (!decodedRange) {
			writeStatus(420, "No article(s) selected");
			return true;
		}

		const auto [beginId, endId] = *decodedRange;
		spdlog::trace("Requested OVER of range {}-{}", beginId, endId);

		auto e = getEngineForCurrentGroup();
		if (!e) {
			writeStatus(420, "No engine for this group (internal error?)");
			return true;
		}

		auto [min, max] = (*e)->getArticleIdRange(*state.currentGroup);

		auto list = (*e)->getArticleRange(beginId, std::min<uint64_t>(max, endId), true);
		if (!list) {
			writeStatus(420, "No article range for this group (internal error?)");
			return true;
		}

		writeStatus(224, "Overview information follows");
		auto guard = gsl::finally([this] () {
			writeLine(".");
		});

		printOverview(*list);
		return true;
	}

	void printOverview(ArticleList& lst) {
		for (const auto& post : lst.getArticles()) {
			writeLine("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}",
					  post->getId(),
					  post->getSubject(),
					  post->getFrom(),
					  asDateString(post->getTimestamp()),
					  post->getMessageId(),
					  generateOverRefList(post->getId()),
					  post->getBytes(),
					  post->getLines(),
					  "Xref: fetchnews");
		}
	}

	// Pretty slow implementation, but works. Best way would probably be to create
	// some kind of references cache in the DB, and simply read the field from the
	// DB here.
	std::string generateOverRefList(uint64_t postId) {
		auto id = postId;
		std::deque<std::string> references;

		auto e = getEngineForCurrentGroup();
		if (!e) return "";

		while (id != 0) {
			auto posts = (*e)->getArticleRange(id, id, false);
			if (!posts || posts->size() != 1)
				break;

			auto post = posts->getArticles()[0];
			references.push_back(post->getMessageId());

			id = post->getParentId();
		}

		references.pop_front();
		return join(references.rbegin(), references.rend());
	}

	bool handleList(std::istream& request) {
		std::string arg;
		request >> arg;
		uppercase(arg);

		if (arg.empty() || arg == "ACTIVE") {
			writeStatus(215,
						"List of newsgroups in form \"group_name last_article_id "
						"first_article_id posting_enabled\"");

			for (const auto& engine : engines.getEngines()) {
				std::vector<std::string> arr;
				engine->populateNewsgroupNames(arr);

				for (const auto& name: arr) {
					const auto [low, high] = engine->getArticleIdRange(name);
					bool posting = engine->postingEnabled(name);

					writeLine("{} {} {} {}", name, high, low,
							  posting ? "y" : "n");
				}
			}

			writeLine(".");
		} else if (arg == "OVERVIEW.FMT") {
			writeStatus(215, "Order of fields in overview database");
			writeLine("Subject:\r\n"
					  "From:\r\n"
					  "Date:\r\n"
					  "Message-ID:\r\n"
					  "References:\r\n"
					  "Bytes:\r\n"
					  "Lines:\r\n"
					  "Xref:full\r\n"
					  ".");
		} else {
			writeStatus(500, "Unknown LIST argument, sorry");
		}

		return true;
	}

	std::optional<EngineRef> getEngineForCurrentGroup() {
		if (!state.currentGroup) return std::nullopt;

		for (auto& e : engines.getEngines()) {
			if (e->acceptsNewsgroupName(*state.currentGroup))
				return e;
		}

		return std::nullopt;
	}

private:
	ip::tcp::socket socket;
	NNTPState state;
	EngineList& engines;
	DatabaseRef db;
};

NNTPServer::NNTPServer(io_context& io, DatabaseRef db, EngineList& engines) : io(io), db(std::move(db)), engines(engines) {
	const auto& conf = Config::get();
	const std::regex r { R"(^nntp://(.+?):([0-9]+)$)" };
	std::smatch m;

	if (!std::regex_match(conf->general.bind, m, r) || m.size() != 3) {
		throw Error::Config::formatted(
			"bind host not recognized, should be in the form: "
			"`nntp://<host>:port` (got: '{}')", conf->general.bind);
	}

	const auto host = m[1].str();
	const auto port = std::strtol(m[2].str().c_str(), nullptr, 10);

	acceptor = std::make_shared<ip::tcp::acceptor>(
		io,
		ip::tcp::endpoint(ip::address_v4::from_string(host), port)
	);

	startAccepting();
}

void NNTPServer::startAccepting() {
	auto conn = NNTPConnection::create(io, db, engines);

	spdlog::trace("Setting up connection listener");
	acceptor->async_accept(conn->getSocket(), [this, conn] (const auto& err) {
		spdlog::trace("Got connection, handling accept");
		handleAccept(conn, err);
	});
}

void NNTPServer::handleAccept(
	std::shared_ptr<NNTPConnection> conn,
	const boost::system::error_code& err
) {
	if (!err) {
		boost::asio::post(threads, [conn] () {
			conn->start();
			spdlog::trace("Exiting connection thread");
		});
	}

	startAccepting();
}
