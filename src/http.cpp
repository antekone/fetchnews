#include <cpr/cpr.h>
#include <spdlog/spdlog.h>
#include <nlohmann/json.hpp>

#include "http.h"
#include "errors.h"

namespace http {

struct HTTPClientPrivate {
};

std::string Result::serialize() {
	nlohmann::json e;

	e["b"] = body;
	e["sc"] = statusCode;

	return e.dump();
}

std::shared_ptr<Result> Result::deserialize(const std::string& input) {
	try {
		auto d = nlohmann::json::parse(input);
		auto r = Result::create();

		r->body = d["b"];
		r->statusCode = d["sc"];

		return r;
	} catch (const nlohmann::json::parse_error& e) {
		throw Error::Internal::formatted("cache deserialization parse error: {}", e.what());
	} catch (const nlohmann::json::type_error& e) {
		throw Error::Internal::formatted("cache deserialization type error: {}", e.what());
	}
}

ResultRef createResult(cpr::Response& response) {
	auto result = Result::create();
	result->body = response.text;
	result->statusCode = response.status_code;
	return result;
}

Client::Client() {
	p = std::make_shared<HTTPClientPrivate>();
}

ResultRef Client::get(const std::string& urlString) {
	spdlog::trace("HTTP GET: {}", urlString);
	auto url = cpr::Url(urlString);

	std::this_thread::sleep_for(std::chrono::seconds(1));

	auto response = cpr::Get(url, cpr::Timeout { 20000 });
	if (response.error) {
		throw Error::Net::formatted("Error during HTTP GET: {} ({})",
									response.error.message,
									response.error.code);
	}

	return createResult(response);
}

ResultRef Client::get(const std::string& urlString, std::shared_ptr<db::Database>& db) {
	auto existing = db->getCacheForURL(urlString);
	if (existing) {
		// TODO: consult cache age -- if it's too old, then don't use it.
		try {
			return Result::deserialize(*existing);
		} catch (const Error::Internal& e) {
			// don't use this cache object, since it doesn't want to
			// deserialize itself. Maybe it's an old cache object from
			// the previous version of the app. Anyway, next call to
			// putCacheForURL should straighten things up.
		}
	}

	auto result = get(urlString);
	auto serialized = result->serialize();
	db->putCacheForURL(urlString, serialized);
	return result;
}

}
