#pragma once

#include <memory>
#include <boost/asio.hpp>

#include "db.h"
#include "helpers.h"
#include "engines/engine.h"

class NNTPConnection;

class NNTPServer {
public:
	static inline Creator<NNTPServer> create;
	explicit NNTPServer(boost::asio::io_context& io, DatabaseRef db, EngineList& engines);

private:
	std::shared_ptr<boost::asio::ip::tcp::acceptor> acceptor;
	boost::asio::io_context& io;
	boost::asio::thread_pool threads;
	DatabaseRef db;
	EngineList& engines;

	void startAccepting();
	void handleAccept(std::shared_ptr<NNTPConnection> conn,
					  const boost::system::error_code& err);
};